import React, { memo } from "react";

function Enfant({ chaine, myFunc }) {
  console.log("Mise à jour du composant");

  return (
    <div>
      <h2>Le composant Enfant</h2>
      <p>{chaine}</p>
      <button onClick={myFunc}>Click</button>
    </div>
  );
}

export default memo(Enfant);
