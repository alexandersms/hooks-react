import React, { useState, useMemo, useCallback } from "react";
import Enfant from "./Enfant";

export default function Content() {
  const [compteur, setCompteur] = useState(0);
  const array = useMemo(() => [1, 2, 3, 4, 5], []);

  const foo = useCallback(() => console.log("Clic"), []);

  return (
    <div>
      <h1>Parent</h1>
      <p>{compteur}</p>
      <button onClick={() => setCompteur(compteur + 2)}>+</button>
      <Enfant chaine={array} myFunc={foo} />
    </div>
  );
}
