import React from "react";
import "../styles/Modal.css";

const Modal = ({ closeMod, stopProp }) => {
  return (
    <div onClick={closeMod} className="overlay">
      <div onClick={stopProp} className="contenu">
        Fenetre Modal
        <button onClick={closeMod} className="btnClose">Fermer</button>
      </div>
    </div>
  );
};

export default Modal;
