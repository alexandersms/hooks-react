import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Compteur from "./components/Compteur";

const App = () => {
  return (
    <div className="App">
      <Navbar />
      <Compteur />
    </div>
  );
};

export default App;
